﻿using UnityEngine;
using System.Collections;

public class Main : MonoBehaviour {
    public Camera FP,TP;
    public bool FirstPerson = true;
	void Start () {
        FP.enabled = FirstPerson;
        TP.enabled = !FirstPerson;

        SwordController.OnCrossClicked += SwordController_OnCrossClicked;
        SwordController.OnSquareClicked += SwitchCamera;
	}

    void SwordController_OnCrossClicked()
    {
        GameObject.Find("Spawner").GetComponent<Spawner>().Respawn();
    }

    void SwitchCamera()
    {
        FirstPerson = !FirstPerson;
        FP.enabled = FirstPerson;
        TP.enabled = !FirstPerson;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
