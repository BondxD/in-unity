﻿using UnityEngine;
using System.Collections;

public class Creator : MonoBehaviour
{
    public Rigidbody brick;
    public float initialSpeed = 1;
    public int frames = 20;
    public int TTL = 10;
    private int ammount = 0;

    void Update()
    {
        if (Time.frameCount % frames == 0)
        {
            Rigidbody rgb = Instantiate(brick, transform.position, Quaternion.identity) as Rigidbody;
            rgb.velocity = transform.forward * initialSpeed;
            Destroy(rgb.gameObject, TTL);
            ammount++;
        }
    }
}
