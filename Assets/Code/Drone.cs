﻿using UnityEngine;
using System.Collections;

public class Drone : MonoBehaviour
{
    private Vector3 target;
    public Vector3 ul;
    public Vector3 dr;

    // Use this for initialization
    void Start()
    {
        target = transform.position;
        ul = target + new Vector3(-3, -2, 0);
        dr = target + new Vector3(3, 1, 0);
        target.x += 1;
        Debug.Log(ul);
        Debug.Log(dr);
        Debug.Log(target);
    }

    void Update()
    {
        float num = Mathf.Sin(Time.frameCount * 0.01f);
        float num2 = Mathf.Sin(Time.frameCount * 0.03f);
        transform.position = target + new Vector3(num, num2, 0);
    }
}
