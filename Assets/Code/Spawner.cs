﻿using UnityEngine;
using System.Collections;

public class Spawner : MonoBehaviour {
    public Rigidbody box;
    public int x=5, y=10;
    private ArrayList spawned = new ArrayList();
    public void Respawn()
    {
        foreach (Rigidbody item in spawned)
        {
            Destroy(item.gameObject);
            Destroy(item);
        }
        spawned.Clear();
        spawn();
    }

	private void spawn () {
        for (int i = 0; i < y; i++)
        {
            for (int j = 0; j < x; j++)
            {
                Rigidbody a = Instantiate(box, new Vector3(transform.position.x + 0.2f * j, transform.position.y + 0.2f * i, transform.position.z), Quaternion.identity) as Rigidbody;
                spawned.Add(a);
            }
        }
	}

}
