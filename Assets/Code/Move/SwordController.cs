using UnityEngine;
using System.Collections;
using System;
using System.IO;

public class SwordController : MonoBehaviour {
    public delegate void ButtonClicked();
    public static event ButtonClicked OnCrossClicked;
    public static event ButtonClicked OnSquareClicked;

    private GameObject sword;
    private UniMoveController move = null;
    private Quaternion initialRotation;
    private ArrayList dataList = new ArrayList(100);
    public bool savingData = false;
    private GUIStyle strongBlackStyle = new GUIStyle();
    private string errorMessage="";

    void Start(){
        try
        {
            Time.maximumDeltaTime = 0.1f;
            sword = gameObject;

            int count = UniMoveController.GetNumConnected();
            if (count == 0) throw new Exception("Move not found");
            else
            {
                move = gameObject.AddComponent<UniMoveController>();
                if (!move.Init(0)) Destroy(move);
                PSMoveConnectionType conn = move.ConnectionType;
                if (conn == PSMoveConnectionType.Unknown || conn == PSMoveConnectionType.USB)
                {
                    Destroy(move);
                }
                move.OnControllerDisconnected += HandleControllerDisconnected;

                move.InitOrientation();
                move.ResetOrientation();

                move.SetLED(Color.yellow);
            }
            initialRotation = sword.transform.rotation;
            move.UpdateRate = 0.02f; //50Hz

            strongBlackStyle.fontSize = 16;
            strongBlackStyle.normal.textColor = Color.black;
        }
        catch (Exception e)
        {
            errorMessage = e.Message;
            Debug.LogError(errorMessage);
        }
	}
    void FixedUpdate(){
        if (move != null)
        {
            if (move.GetButtonDown(PSMoveButton.Move))
            {
                move.ResetOrientation();
            }
            if (move.GetButtonDown(PSMoveButton.Cross) && OnCrossClicked != null)
            {
                OnCrossClicked();
            }
            if (move.GetButtonDown(PSMoveButton.Square) && OnSquareClicked != null)
            {
                OnSquareClicked();
            }
            sword.transform.rotation = initialRotation * move.Orientation;
            if (savingData)
                dataList.Add(move.Acceleration);
        }
    }

    void HandleControllerDisconnected(object sender, EventArgs e)
    {
        // TODO: Remove this disconnected controller from the list and maybe give an update to the player
    }
    void OnGUI()
    {
        if (move != null)
        {
            //string display = string.Format("PS Move {0}:\n ax:{1:0.000}, ay:{2:0.000}, az:{3:0.000}\n gx:{4:0.000}, gy:{5:0.000}, gz:{6:0.000}\n mx:{7:0.000}, my:{8:0.000}, mz:{9:0.000}\n rotx:{10:0.000}, roty:{11:0.000}, rotz:{12:0.000}\n",
            //        0, move.Acceleration.x, move.Acceleration.y, move.Acceleration.z,
            //        move.Gyro.x, move.Gyro.y, move.Gyro.z,
            //        move.Magnetometer.x, move.Magnetometer.y, move.Magnetometer.z,
            //        move.Orientation.eulerAngles.x, move.Orientation.eulerAngles.y, move.Orientation.eulerAngles.z);
            //GUI.Label(new Rect(10, Screen.height - 100, 500, 100), display, strongBlackStyle);
            GUI.Label(new Rect(5, 5, 100, 100), string.Format("fps: {0}", Math.Round(1.0f / Time.smoothDeltaTime)),strongBlackStyle);
        }
        GUI.Label(new Rect(5, 5, 100, 100), errorMessage, strongBlackStyle);
    }

    void OnDestroy()
    {
        if (savingData)
        {
            print("Saving...");
            save(dataList, "normal.csv");
            print("Saved");
        }
    }

    private void save(ArrayList list, String name)
    {
        using (StreamWriter theWriter = File.CreateText("../Data/"+name))
        {
           theWriter.WriteLine("X,Y,Z");
           foreach(Vector3 accel in list)
           {
              theWriter.WriteLine(string.Format("{0},{1},{2}",accel.x, accel.y, accel.z));
           }
        }
    }
}
